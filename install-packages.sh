#!/bin/sh
#promt sudo permissions from user
[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"
IP=$1
#check for valid ip 
if ! expr "$IP" : '[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\:[0-9][0-9]*$'>/dev/null; then
        echo Please enter a valid IP and port for openeis server. Aborting...
    return 
fi

#echo update system...
sudo apt-get update 
sudo apt-get upgrade

#echo install packages...
sudo apt-get install --no-install-recommends xserver-xorg x11-xserver-utils xinit openbox chromium-browser

echo configure system...
CONF="# Disable any form of screen saver / screen blanking / power management
xset s off
xset s noblank
xset -dpms

# Allow quitting the X server with CTRL-ATL-Backspace
setxkbmap -option terminate:ctrl_alt_bksp

# Start Chromium in kiosk mode
sed -i 's/\"exited_cleanly\":false/\"exited_cleanly\":true/' ~/.config/chromium/'Local State'
sed -i 's/\"exited_cleanly\":false/\"exited_cleanly\":true/; s/\"exit_type\":\"[^\"]\+\"/\"exit_type\":\"Normal\"/' ~/.config/chromium/Default/Preferences
chromium-browser --disable-infobars --kiosk $IP"
echo "$CONF">/etc/xdg/openbox/autostart

#Configure autostart when rebooting
#Make sure command is only executed once
RESTART_CMD="[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx -- -nocursor"
PROFILE=~/.profile
if ! grep -q "$RESTART_CMD" "$PROFILE"; then
  echo configure autostart...
  echo $RESTART_CMD >> "$PROFILE"
fi
echo rebooting system...
reboot now
