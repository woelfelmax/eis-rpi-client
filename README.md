# eis-rpi-client

A script for setting up a raspberry pi to work as openeis-client. Check this [reference](https://die-antwort.eu/techblog/2017-12-setup-raspberry-pi-for-kiosk-mode/). The script was tested on a clean raspbian lite installation on rpi4.

## Setup raspberry pi

[Download](https://www.raspberrypi.com/software/operating-systems/) the latest raspbian lite image and make sure to configure the following settings before executing the script:

- Localisation Options: Select your preferred locale (we simply keep the default en_GB.UTF-8), timezone, and keyboard layout.
- Change User Password: This is important – keeping the default password means your Pi will get owned faster than you can say “botnet” as soon as you connect it to the internet. (Make sure to have selected the correct keyboard layout before typing in the new password, though.)
- Network Options: Configure WiFi as needed. Alternatively, you also configure WiFi manually using wpa_passphrase if you don’t want your WiFi password stored on the Pi in clear text.
- Boot Options: Select “Desktop / CLI” and then “Console Autologin”. We’ll come back to this later.
  Interfacing Options: Enable SSH access if needed.
- Advanced Options: Disable “Overscan” if the Pi’s output does not fill your screen completely.

## Execute the script

Clone the script to your working directory on your rpi and execute it. The script prompts the ip from the server the browser shall connect. Make sure to provide the correct ip of the openeis server.
